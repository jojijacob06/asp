#include <stdio.h>
#include <string.h>

#include "KeyValuePair.h"
#include "LinkedList.h"

#define MAX_KEY_SIZE  100
#define MAX_KEY_VALUE_PAIR_SIZE 105

// for use in scanf
#define XSTR(A) STR(A)
#define STR(A) #A


void postKeyValuePairInList(LinkedList_t **head, LinkedList_t **tail, char *key, int value) {
  LinkedList_t *runner = *head;

  while (runner) {
    if (!strcmp(key, runner->pKeyValuePair->key))
      break;
    runner = runner->next;
  }

  if (runner)
    runner->pKeyValuePair->value++;
  else {
    KeyValuePair_t *pNewKeyValuePair = makeNewKeyValuePair(key, value);
    addToListAtTail(head, tail, pNewKeyValuePair);
  }
}

void getKeyAndValueFromString(char *keyValuePairAsString, char *key, int *value) {
  char *runner = keyValuePairAsString;
  int keyLength = 0;

  *value = 0;

  while (',' != *(++runner))
    key[keyLength++] = *runner;

  key[keyLength] = '\0';

  while (')' != *(++runner))
    *value = *value * 10 + (*runner-'0');
}

int main () {
  LinkedList_t* head[26] = {NULL, };
  LinkedList_t* tail[26] = {NULL, };
  char startingLetter = 'a';
  char keyValuePairAsString[MAX_KEY_VALUE_PAIR_SIZE+1];

  scanf("%"XSTR(MAX_KEY_VALUE_PAIR_SIZE)"s", keyValuePairAsString);

  startingLetter = keyValuePairAsString[1];

  do {
    char key[MAX_KEY_SIZE];
    int value;
    getKeyAndValueFromString(keyValuePairAsString, key, &value);
    if (startingLetter != keyValuePairAsString[1]) {
      printList(head[startingLetter-'a']);
      freeList(&head[startingLetter-'a'], &tail[startingLetter-'a']);
      startingLetter = keyValuePairAsString[1];
    }
    postKeyValuePairInList(&head[startingLetter-'a'], &tail[startingLetter-'a'], key, value);
  }while(EOF != scanf("%"XSTR(MAX_KEY_VALUE_PAIR_SIZE)"s", keyValuePairAsString));

  printList(head[startingLetter-'a']);
  freeList(&head[startingLetter-'a'], &tail[startingLetter-'a']);
  
  return 0;
}
