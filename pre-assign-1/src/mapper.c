#include <stdio.h>

#include "KeyValuePair.h"

#define MAX_KEY_SIZE  100

// for use in scanf
#define XSTR(A) STR(A)
#define STR(A) #A

int main () {
  char key[MAX_KEY_SIZE+1];

  while (EOF != scanf("%"XSTR(MAX_KEY_SIZE)"s", key)) {
    KeyValuePair_t *pKeyValuePair = makeNewKeyValuePair(key, 1);
    prettyPrintKeyValuePair(pKeyValuePair);
    freeKeyValuePair(pKeyValuePair);
  }

  return 0;
}
