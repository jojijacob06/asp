#include <stdio.h>
#include <stdlib.h>
#include "LinkedList.h"

void addToListAtHead(LinkedList_t **head, LinkedList_t **tail, KeyValuePair_t *pNewKeyValuePair) {
  LinkedList_t *newElement = (LinkedList_t*) malloc(sizeof(LinkedList_t));
  newElement->pKeyValuePair = pNewKeyValuePair;
  newElement->next = NULL;

  if (NULL == *head) {
    *head = newElement;
    *tail = newElement;
  }
  else {
    newElement->next = *head;
    *head = newElement;
  }
}

void addToListAtTail(LinkedList_t **head, LinkedList_t **tail, KeyValuePair_t *pNewKeyValuePair) {
  LinkedList_t *newElement = (LinkedList_t*) malloc(sizeof(LinkedList_t));
  newElement->pKeyValuePair = pNewKeyValuePair;
  newElement->next = NULL;

  if (NULL == *tail) {
    *head = newElement;
    *tail = newElement;
  }
  else {
    (*tail)->next = newElement;
    *tail = newElement;
  }
}

void freeList(LinkedList_t **head, LinkedList_t **tail) {
  LinkedList_t *runner = *head;
  LinkedList_t *nextRunner = NULL;

  while (runner) {
    nextRunner = runner->next;
    freeKeyValuePair(runner->pKeyValuePair);
    free(runner);
    runner = nextRunner;
  }

  *head = NULL;
  *tail = NULL;
}

void printList(LinkedList_t *head) {
  LinkedList_t *runner = head;

  while (runner) {
    prettyPrintKeyValuePair(runner->pKeyValuePair);
    runner = runner->next;
  }
}