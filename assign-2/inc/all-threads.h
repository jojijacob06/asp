#ifndef ALL_THREADS_H
#define ALL_THREADS_H

#include <LinkedList.h>
#include <prod-cons.h>

void startAllThreads(pthread_t *mapperPoolUpdaterThread, 
                     pthread_t *mapperThread, 
                     pthread_t *reducerThread, 
                     pthread_t *summarizerThread, 
                     pthread_t *wordCountWriterThread, 
                     pthread_t *letterCountWriterThread,
                     stats_t   *mapperPoolUpdaterStats,
                     stats_t   *mapperStats,
                     stats_t   *reducerStats,
                     stats_t   *summarizerStats,
                     stats_t   *wordCountWriterStats,
                     stats_t   *letterCountWriterStats);                    
void waitForAllThreads(pthread_t mapperPoolUpdaterThread, 
                       pthread_t *mapperThread, 
                       pthread_t *reducerThread, 
                       pthread_t *summarizerThread, 
                       pthread_t wordCountWriterThread, 
                       pthread_t letterCountWriterThread);
void *mapperPoolUpdater(void *null);
void doMapperWork(LinkedList_t *head, LinkedList_t **newHead, LinkedList_t **newTail);
void *mapper(void *id);
void postKeyValuePairInList(LinkedList_t **head, LinkedList_t **tail, char *key, int value);
void doReducerWork(LinkedList_t *head, LinkedList_t **newHead, LinkedList_t **newTail);
void *reducer(void *id);
void doSummarizerWork(LinkedList_t *head, LinkedList_t **newHead, LinkedList_t **newTail);
void *summarizer(void *id);
void *wordCountWriter(void *null);
void *letterCountWriter(void *null);

#endif