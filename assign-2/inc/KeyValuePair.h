#ifndef KEYVALUEPAIR_H
#define KEYVALUEPAIR_H

#include <stdio.h>

typedef struct KeyValuePair {
  char *key;
  int value;
}KeyValuePair_t;

KeyValuePair_t* makeNewKeyValuePair(char *key, int value);
void freeKeyValuePair(KeyValuePair_t *pKeyValuePair);
void prettyPrintKeyValuePair(KeyValuePair_t *pKeyValuePair);
void prettyPrintKeyValuePairToFile(KeyValuePair_t *pKeyValuePair, FILE *fp);

#endif