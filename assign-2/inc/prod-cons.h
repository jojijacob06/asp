#ifndef PROD_CONS_H
#define PROD_CONS_H

#include <pthread.h>

#include <assign-2.h>
#include <LinkedList.h>

#define MAX_BUFFER_SIZE                 13

#define PROD_FINISHED                   0xAA
#define PROD_NOT_FINISHED               0xBB

#define MAPPER_POOL_UPDATER_TYPE_THREAD 0x00
#define MAPPER_TYPE_THREAD              0x01
#define REDUCER_TYPE_THREAD             0x02
#define SUMMARIZER_TYPE_THREAD          0x03
#define WORD_COUNT_WRITER_TYPE_THREAD   0x04
#define LETTER_COUNT_WRITER_TYPE_THREAD 0x05

typedef struct {
  LinkedList_t*     head[MAX_BUFFER_SIZE];
  LinkedList_t*     tail[MAX_BUFFER_SIZE];
  int               occupied;
  int               nextin;
  int               nextout;
  int               producersFinished;
  int               productionStatus;
  pthread_mutex_t   mutex;
  pthread_cond_t    more;
  pthread_cond_t    less;
}buffer_t;

typedef struct {
  int tid;
  long prod;
  long cons;
} stats_t;

void producer(buffer_t *b, LinkedList_t *head, LinkedList_t *tail);
void producerFinished(buffer_t *b, int producerType);
int consumer(buffer_t *b, LinkedList_t **head, LinkedList_t **tail);

#endif
