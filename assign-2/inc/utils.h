#ifndef UTILS_H
#define UTILS_H

#include <assign-2.h>

void checkArgsAndAssignGlobals(char *argv[]);
void initBuffer(buffer_t *b);
void initBuffers();

#endif
