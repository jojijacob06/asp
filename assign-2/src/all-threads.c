#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>

#include <assign-2.h>
#include <LinkedList.h>
#include <KeyValuePair.h>
#include <all-threads.h>
#include <prod-cons.h>

extern char *filename;
extern int numberOfMapperThreads;
extern int numberOfReducerThreads;
extern int numberOfSummarizerThreads;

extern buffer_t mapperPoolBuffer;
extern buffer_t reducerPoolBuffer;
extern buffer_t summarizerPoolBuffer;
extern buffer_t wordCountPoolBuffer;
extern buffer_t letterCountPoolBuffer;

void startAllThreads(pthread_t *mapperPoolUpdaterThread, 
                     pthread_t *mapperThread, 
                     pthread_t *reducerThread, 
                     pthread_t *summarizerThread, 
                     pthread_t *wordCountWriterThread, 
                     pthread_t *letterCountWriterThread,
                     stats_t   *mapperPoolUpdaterStats,
                     stats_t   *mapperStats,
                     stats_t   *reducerStats,
                     stats_t   *summarizerStats,
                     stats_t   *wordCountWriterStats,
                     stats_t   *letterCountWriterStats) {
    int rc;
    
    rc = pthread_create(mapperPoolUpdaterThread, NULL, mapperPoolUpdater, (void *)mapperPoolUpdaterStats);
    if (rc) {
      fprintf(stderr, "ERROR; pthread_create() returned %s\n", strerror(rc));
      exit(-1);
    }
  
    for (int i=0; i<numberOfMapperThreads; i++) {
      rc = pthread_create(&mapperThread[i], NULL, mapper, (void *)(&mapperStats[i]));
      if (rc) {
        fprintf(stderr, "ERROR; pthread_create() returned %s\n", strerror(rc));
        exit(-1);
      }  
    }
  
    for (int i=0; i<numberOfReducerThreads; i++) {
      rc = pthread_create(&reducerThread[i], NULL, reducer, (void *)(&reducerStats[i]));
      if (rc) {
        fprintf(stderr, "ERROR; pthread_create() returned %s\n", strerror(rc));
        exit(-1);
      }  
    }
  
    for (int i=0; i<numberOfSummarizerThreads; i++) {
      rc = pthread_create(&summarizerThread[i], NULL, summarizer, (void *)(&summarizerStats[i]));
      if (rc) {
        fprintf(stderr, "ERROR; pthread_create() returned %s\n", strerror(rc));
        exit(-1);
      }  
    }
  
    rc = pthread_create(wordCountWriterThread, NULL, wordCountWriter, (void *)wordCountWriterStats);
    if (rc) {
      fprintf(stderr, "ERROR; pthread_create() returned %s\n", strerror(rc));
      exit(-1);
    }

    rc = pthread_create(letterCountWriterThread, NULL, letterCountWriter, (void *)letterCountWriterStats);
    if (rc) {
      fprintf(stderr, "ERROR; pthread_create() returned %s\n", strerror(rc));
      exit(-1);
    }
}

void waitForAllThreads(pthread_t mapperPoolUpdaterThread, 
                       pthread_t *mapperThread, 
                       pthread_t *reducerThread, 
                       pthread_t *summarizerThread, 
                       pthread_t wordCountWriterThread, 
                       pthread_t letterCountWriterThread) {
                         
  pthread_join(mapperPoolUpdaterThread, NULL);
  for (int i=0; i<numberOfMapperThreads; i++) {
    pthread_join(mapperThread[i], NULL);
  }
  for (int i=0; i<numberOfReducerThreads; i++) {
    pthread_join(reducerThread[i], NULL);
  }
  for (int i=0; i<numberOfSummarizerThreads; i++) {
    pthread_join(summarizerThread[i], NULL);
  }
  pthread_join(wordCountWriterThread, NULL);
  pthread_join(letterCountWriterThread, NULL);  
}

void *mapperPoolUpdater(void *inStats) {
  char key[MAX_KEY_SIZE];
  FILE *fp;
  char startingLetter = 'a';
  LinkedList_t* head = NULL;
  LinkedList_t* tail = NULL;
  KeyValuePair_t *pNewKeyValuePair = NULL;
  stats_t *stats = (stats_t *)inStats;
  
  if (!(fp = fopen(filename, "r"))) {
    fprintf(stderr, "Fopen failed to open %s(%s).\n", filename, strerror(errno));
    pthread_exit((void *)EXIT_FAILURE);
  }
  
  fscanf(fp, "%"XSTR(MAX_KEY_SIZE)"s", key);
  
  startingLetter = key[0];
  
  do {
    pNewKeyValuePair = makeNewKeyValuePair(key, -1);
    if (startingLetter != key[0]) {
      producer(&mapperPoolBuffer, head, tail);
      stats->prod++;
      startingLetter = key[0];
      head = NULL;
      tail = NULL;
    }
    addToListAtTail(&head, &tail, pNewKeyValuePair);
  }while (EOF != fscanf(fp, "%"XSTR(MAX_KEY_SIZE)"s", key));
  
  producer(&mapperPoolBuffer, head, tail);
  stats->prod++;
    
  producerFinished(&mapperPoolBuffer, MAPPER_POOL_UPDATER_TYPE_THREAD);
  
  fclose (fp);
  
  pthread_exit(NULL);
}

void doMapperWork(LinkedList_t *head, LinkedList_t **newHead, LinkedList_t **newTail) {
  LinkedList_t *runner = head;
  KeyValuePair_t *pNewKeyValuePair = NULL;
  *newHead = NULL;
  *newTail = NULL;
  
  while (runner) {
    pNewKeyValuePair = makeNewKeyValuePair(runner->pKeyValuePair->key, 1);
    addToListAtTail(newHead, newTail, pNewKeyValuePair);
    runner = runner->next;
  }
}

void *mapper(void *inStats) {
  stats_t *stats = (stats_t *)inStats;
  
  while (1) {
    LinkedList_t* headFromMapperPoolBuffer = NULL;
    LinkedList_t* tailFromMapperPoolBuffer = NULL;
    LinkedList_t* headToReducerPoolBuffer = NULL;
    LinkedList_t* tailToReducerPoolBuffer = NULL;

    // printf("in mapper %d\n", tid);
    
    if (PROD_FINISHED == consumer(&mapperPoolBuffer, &headFromMapperPoolBuffer, &tailFromMapperPoolBuffer))
      break;
    stats->cons++;
    
    doMapperWork(headFromMapperPoolBuffer, &headToReducerPoolBuffer, &tailToReducerPoolBuffer);
    producer(&reducerPoolBuffer, headToReducerPoolBuffer, tailToReducerPoolBuffer);
    stats->prod++;
    freeList(&headFromMapperPoolBuffer, &tailFromMapperPoolBuffer);
  }
  
  producerFinished(&reducerPoolBuffer, MAPPER_TYPE_THREAD);
  
  pthread_exit(NULL);
}

void postKeyValuePairInList(LinkedList_t **head, LinkedList_t **tail, char *key, int value) {
  LinkedList_t *runner = *head;

  while (runner) {
    if (!strcmp(key, runner->pKeyValuePair->key))
      break;
    runner = runner->next;
  }

  if (runner)
    runner->pKeyValuePair->value++;
  else {
    KeyValuePair_t *pNewKeyValuePair = makeNewKeyValuePair(key, value);
    addToListAtTail(head, tail, pNewKeyValuePair);
  }
}

void doReducerWork(LinkedList_t *head, LinkedList_t **newHead, LinkedList_t **newTail) {
  LinkedList_t *runner = head;
  *newHead = NULL;
  *newTail = NULL;
  
  while (runner) {
    postKeyValuePairInList(newHead, newTail, runner->pKeyValuePair->key, runner->pKeyValuePair->value);
    runner = runner->next;
  }
}

void *reducer(void *inStats) {
  stats_t *stats = (stats_t *)inStats;
  
  while (1) {
    LinkedList_t* headFromReducerPoolBuffer = NULL;
    LinkedList_t* tailFromReducerPoolBuffer = NULL;
    LinkedList_t* headToSummarizerPoolBuffer = NULL;
    LinkedList_t* tailToSummarizerPoolBuffer = NULL;
    
    // printf("in reducer %d\n", tid);
    
    if (PROD_FINISHED == consumer(&reducerPoolBuffer, &headFromReducerPoolBuffer, &tailFromReducerPoolBuffer))
      break;
    stats->cons++;
    
    doReducerWork(headFromReducerPoolBuffer, &headToSummarizerPoolBuffer, &tailToSummarizerPoolBuffer);
    producer(&summarizerPoolBuffer, headToSummarizerPoolBuffer, tailToSummarizerPoolBuffer);
    stats->prod++;
    // printList(headToSummarizerPoolBuffer);
    freeList(&headFromReducerPoolBuffer, &tailFromReducerPoolBuffer);
  }
  
  producerFinished(&summarizerPoolBuffer, REDUCER_TYPE_THREAD);
  
  pthread_exit(NULL);
}

void doSummarizerWork(LinkedList_t *head, LinkedList_t **newHead, LinkedList_t **newTail) {
  LinkedList_t *runner = head;
  KeyValuePair_t *pNewKeyValuePair = NULL;
  *newHead = NULL;
  *newTail = NULL;
  int letterCount = 0;
  char letter[2] = "";
  
  if (runner) {
    strncpy(letter, runner->pKeyValuePair->key, 1);
    
    while (runner) {
      letterCount += runner->pKeyValuePair->value;
      runner = runner->next;
    }
  
    pNewKeyValuePair = makeNewKeyValuePair(letter, letterCount);
    
    addToListAtTail(newHead, newTail, pNewKeyValuePair);
  }
}

void *summarizer(void *inStats) {
  stats_t *stats = (stats_t *)inStats;
  
  while (1) {
    LinkedList_t* headFromSummarizerPoolBuffer = NULL;
    LinkedList_t* tailFromSummarizerPoolBuffer = NULL;
    LinkedList_t* headToLetterCountPoolBuffer = NULL;
    LinkedList_t* tailToLetterCountPoolBuffer = NULL;
    
    if (PROD_FINISHED == consumer(&summarizerPoolBuffer, &headFromSummarizerPoolBuffer, &tailFromSummarizerPoolBuffer))
      break;
    stats->cons++;
    
    // printf("in summarizer %d\n", tid);
    doSummarizerWork(headFromSummarizerPoolBuffer, &headToLetterCountPoolBuffer, &tailToLetterCountPoolBuffer);
    producer(&wordCountPoolBuffer, headFromSummarizerPoolBuffer, tailFromSummarizerPoolBuffer);
    stats->prod++;
    producer(&letterCountPoolBuffer, headToLetterCountPoolBuffer, tailToLetterCountPoolBuffer);
    stats->prod++;
    // printList(headToLetterCountPoolBuffer);
  }
  
  producerFinished(&wordCountPoolBuffer, SUMMARIZER_TYPE_THREAD);
  producerFinished(&letterCountPoolBuffer, SUMMARIZER_TYPE_THREAD);
  
  pthread_exit(NULL);
}

void *wordCountWriter(void *inStats) {
  FILE *fp;
  stats_t *stats = (stats_t *)inStats;
  
  if (!(fp = fopen("wordCount.txt", "w"))) {
    fprintf(stderr, "Fopen failed to open wordCount.txt(%s).\n", strerror(errno));
    pthread_exit((void *)EXIT_FAILURE);
  }
  
  while (1) {
    LinkedList_t* headFromWordCountPoolBuffer = NULL;
    LinkedList_t* tailFromWordCountPoolBuffer = NULL;
    
    if (PROD_FINISHED == consumer(&wordCountPoolBuffer, &headFromWordCountPoolBuffer, &tailFromWordCountPoolBuffer))
      break;
    stats->cons++;
    
    printListToFile(headFromWordCountPoolBuffer, fp);
    freeList(&headFromWordCountPoolBuffer, &tailFromWordCountPoolBuffer);
  }
  
  fclose (fp);
  
  pthread_exit(NULL);
}

void *letterCountWriter(void *inStats) {
  FILE *fp;
  stats_t *stats = (stats_t *)inStats;
  
  if (!(fp = fopen("letterCount.txt", "w"))) {
    fprintf(stderr, "Fopen failed to open letterCount.txt(%s).\n", strerror(errno));
    pthread_exit((void *)EXIT_FAILURE);
  }
  
  while (1) {
    LinkedList_t* headFromLetterCountPoolBuffer = NULL;
    LinkedList_t* tailFromLetterCountPoolBuffer = NULL;
    
    if (PROD_FINISHED == consumer(&letterCountPoolBuffer, &headFromLetterCountPoolBuffer, &tailFromLetterCountPoolBuffer))
      break;
    stats->cons++;
    
    printListToFile(headFromLetterCountPoolBuffer, fp);
    freeList(&headFromLetterCountPoolBuffer, &tailFromLetterCountPoolBuffer);
  }
  
  fclose (fp);
  
  pthread_exit(NULL);
}