#include <pthread.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include <prod-cons.h>

extern char *filename;
extern int numberOfMapperThreads;
extern int numberOfReducerThreads;
extern int numberOfSummarizerThreads;

extern buffer_t mapperPoolBuffer;
extern buffer_t reducerPoolBuffer;
extern buffer_t summarizerPoolBuffer;
extern buffer_t wordCountPoolBuffer;
extern buffer_t letterCountPoolBuffer;

void checkArgsAndAssignGlobals(char *argv[]) {
  FILE *fp;
  errno = 0;
  fp = fopen(argv[1], "r");
  if (errno) {
    fprintf (stderr, "Bad argument - %s\n", strerror(errno));
    exit (EXIT_FAILURE);
  }
  numberOfMapperThreads = strtoimax(argv[2], NULL, 10);
  numberOfReducerThreads = strtoimax(argv[3], NULL, 10);
  numberOfSummarizerThreads = strtoimax(argv[4], NULL, 10);
  if (errno) {
    fprintf (stderr, "Bad number of thread(s) - %s\n", strerror(errno));
    exit (EXIT_FAILURE);
  }
  if (!numberOfMapperThreads || !numberOfReducerThreads || !numberOfSummarizerThreads) {
    fprintf (stderr, "Bad number of thread(s)\n");
    exit (EXIT_FAILURE);
  }
  fclose(fp);
  filename = argv[1];
}

void initBuffer(buffer_t *b) {
  for (int i=0; i<MAX_BUFFER_SIZE; i++) {
    b->head[i] = NULL;
    b->tail[i] = NULL;
  }
  b->occupied = 0;
  b->nextin = 0;
  b->nextout = 0;
  b->producersFinished = 0;
  b->productionStatus = PROD_NOT_FINISHED;
  pthread_mutex_init(&b->mutex, NULL);
  pthread_cond_init(&b->more, NULL);
  pthread_cond_init(&b->less, NULL);
}

void initBuffers() {
  initBuffer (&mapperPoolBuffer);
  initBuffer (&reducerPoolBuffer);
  initBuffer (&summarizerPoolBuffer);
  initBuffer (&wordCountPoolBuffer);
  initBuffer (&letterCountPoolBuffer);
}