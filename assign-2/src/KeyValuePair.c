#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "KeyValuePair.h"

KeyValuePair_t* makeNewKeyValuePair(char *key, int value) {
  KeyValuePair_t *newKeyValuePair = (KeyValuePair_t*) malloc(sizeof(KeyValuePair_t));
  newKeyValuePair->key = (char *) malloc(strlen(key) + 1);
  strcpy(newKeyValuePair->key, key);
  newKeyValuePair->value = value;

  return newKeyValuePair;
}

void freeKeyValuePair(KeyValuePair_t *pKeyValuePair) {
  free(pKeyValuePair->key);
  free(pKeyValuePair);
}

void prettyPrintKeyValuePair(KeyValuePair_t *pKeyValuePair) {
  printf("(%s,%d)\n", pKeyValuePair->key, pKeyValuePair->value);
}

void prettyPrintKeyValuePairToFile(KeyValuePair_t *pKeyValuePair, FILE *fp) {
  fprintf(fp, "(%s,%d)\n", pKeyValuePair->key, pKeyValuePair->value);
}