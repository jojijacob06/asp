#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <assign-2.h>
#include <LinkedList.h>
#include <KeyValuePair.h>
#include <prod-cons.h>
#include <all-threads.h>
#include <utils.h>

char *filename = NULL;
int numberOfMapperPoolUpdaterThreads = 1;
int numberOfMapperThreads = 0;
int numberOfReducerThreads = 0;
int numberOfSummarizerThreads = 0;

buffer_t mapperPoolBuffer;
buffer_t reducerPoolBuffer;
buffer_t summarizerPoolBuffer;
buffer_t wordCountPoolBuffer;
buffer_t letterCountPoolBuffer;

int main (int argc, char *argv[]) {
  FILE *fp;
  
  pthread_t mapperPoolUpdaterThread;
  pthread_t *mapperThread = NULL;
  pthread_t *reducerThread = NULL;
  pthread_t *summarizerThread = NULL;
  pthread_t wordCountWriterThread;
  pthread_t letterCountWriterThread;
  
  stats_t mapperPoolUpdaterStats = {0, 0, 0};
  stats_t *mapperStats=NULL;
  stats_t *reducerStats=NULL;
  stats_t *summarizerStats=NULL;
  stats_t wordCountWriterStats = {0, 0, 0};
  stats_t letterCountWriterStats = {0, 0, 0};
  
  if (5 != argc) {
    fprintf (stderr, "Wrong usage!\n%s <input filename> <mapper threads> <reducer threads> <summarizer threads>\n", argv[0]);
    exit (1);
  }
  
  checkArgsAndAssignGlobals(argv);
  
  mapperThread = (pthread_t *)malloc(sizeof(pthread_t)*numberOfMapperThreads);
  reducerThread = (pthread_t *)malloc(sizeof(pthread_t)*numberOfReducerThreads);
  summarizerThread = (pthread_t *)malloc(sizeof(pthread_t)*numberOfSummarizerThreads);
  
  mapperStats = (stats_t *)calloc(numberOfMapperThreads, sizeof(stats_t));
  reducerStats = (stats_t *)calloc(numberOfReducerThreads, sizeof(stats_t));
  summarizerStats = (stats_t *)calloc(numberOfSummarizerThreads, sizeof(stats_t));
  
  initBuffers();
  
  startAllThreads(&mapperPoolUpdaterThread, 
                  mapperThread, 
                  reducerThread, 
                  summarizerThread, 
                  &wordCountWriterThread, 
                  &letterCountWriterThread,
                  &mapperPoolUpdaterStats,
                  mapperStats,
                  reducerStats,
                  summarizerStats,
                  &wordCountWriterStats,
                  &letterCountWriterStats);
                  
  waitForAllThreads(mapperPoolUpdaterThread, 
                    mapperThread, 
                    reducerThread, 
                    summarizerThread, 
                    wordCountWriterThread, 
                    letterCountWriterThread);

  if (!(fp = fopen("stats.txt", "w"))) {
    fprintf(stderr, "Fopen failed to open stats.txt(%s).\n", strerror(errno));
    exit (1);
  }
  
  for (int i=0; i<numberOfMapperThreads; i++)
    fprintf (fp, "Mapper%d:     Prod - %ld, Cons - %ld\n", i, mapperStats[i].prod, mapperStats[i].cons);
  fprintf(fp, "\n");
  for (int i=0; i<numberOfReducerThreads; i++)
    fprintf (fp, "Reducer%d:    Prod - %ld, Cons - %ld\n", i, reducerStats[i].prod, reducerStats[i].cons);
  fprintf(fp, "\n");
  for (int i=0; i<numberOfSummarizerThreads; i++)
    fprintf (fp, "Summarizer%d: Prod - %ld, Cons - %ld\n", i, summarizerStats[i].prod, summarizerStats[i].cons);
  fclose (fp);
  
  free (mapperThread);
  free (reducerThread);
  free (summarizerThread);
  free (mapperStats);
  free (reducerStats);
  free (summarizerStats);
                          
  return 0;
}
