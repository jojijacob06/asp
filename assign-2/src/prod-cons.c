#include <assign-2.h>

#include <prod-cons.h>
#include <LinkedList.h>

extern int numberOfMapperPoolUpdaterThreads;
extern int numberOfMapperThreads;
extern int numberOfReducerThreads;
extern int numberOfSummarizerThreads;

void producer(buffer_t *b, LinkedList_t *head, LinkedList_t *tail) {
  pthread_mutex_lock(&b->mutex);
  
  while (b->occupied >= MAX_BUFFER_SIZE)
    pthread_cond_wait(&b->less, &b->mutex);

  b->head[b->nextin] = head;
  b->tail[b->nextin++] = tail;

  b->nextin %= MAX_BUFFER_SIZE;
  b->occupied++;

  pthread_cond_signal(&b->more);
  pthread_mutex_unlock(&b->mutex);
}

void producerFinished(buffer_t *b, int producerType) {
  pthread_mutex_lock(&b->mutex);
  
  b->producersFinished++;
  
  switch (producerType) {
    case MAPPER_POOL_UPDATER_TYPE_THREAD:
      if (numberOfMapperPoolUpdaterThreads == b->producersFinished) {
        b->productionStatus = PROD_FINISHED;
        pthread_cond_signal(&b->more);
      }
      break;
      
    case MAPPER_TYPE_THREAD:
      if (numberOfMapperThreads == b->producersFinished) {
        b->productionStatus = PROD_FINISHED;
        pthread_cond_signal(&b->more);
      }
      break;

    case REDUCER_TYPE_THREAD:
      if (numberOfReducerThreads == b->producersFinished) {
        b->productionStatus = PROD_FINISHED;
        pthread_cond_signal(&b->more);
      }
      break;

    case SUMMARIZER_TYPE_THREAD:
      if (numberOfSummarizerThreads == b->producersFinished) {
        b->productionStatus = PROD_FINISHED;
        pthread_cond_signal(&b->more);
      }
      break;
    
    default:
      break;
  }

  pthread_mutex_unlock(&b->mutex);
}

int consumer(buffer_t *b, LinkedList_t **head, LinkedList_t **tail) {
  int ret = PROD_NOT_FINISHED;
  
  pthread_mutex_lock(&b->mutex);
  
  while(b->occupied <= 0 && b->productionStatus == PROD_NOT_FINISHED)
    pthread_cond_wait(&b->more, &b->mutex);
  
  if (b->occupied > 0) {
    *head = b->head[b->nextout];
    *tail = b->tail[b->nextout++];
  
    b->nextout %= MAX_BUFFER_SIZE;
    b->occupied--;
    
    pthread_cond_signal(&b->less);
    pthread_mutex_unlock(&b->mutex);
  }
  else {
    pthread_cond_signal(&b->more);
    pthread_mutex_unlock(&b->mutex);
    ret = PROD_FINISHED;
  }

  return ret;
}
