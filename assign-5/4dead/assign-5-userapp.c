#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>

#define CDRV_IOC_MAGIC 'Z'
#define E2_IOCMODE1 _IOWR(CDRV_IOC_MAGIC, 1, int)
#define E2_IOCMODE2 _IOWR(CDRV_IOC_MAGIC, 2, int)

#define DEV_FILENAME "/dev/a5"

#define A_STRING DEV_FILENAME

void * do_thread_work_write (void *arg) {
  int *fd = (int *)arg;
    
  sleep (3);
  
  while (1) {
    printf ("Trying to write %d bytes\n", strlen(A_STRING));
    printf ("%d bytes written\n", write (*fd, A_STRING, strlen(A_STRING)));
  }
  
  pthread_exit(NULL);
}

void * do_thread_work_read (void *arg) {
  int *fd = (int *)arg;
  
  char buffer[10];
    
  sleep (3);
  
  while (1) {
    printf ("Trying to read %d bytes\n", sizeof (buffer));
    printf ("%d bytes read\n", read (*fd, buffer, sizeof (buffer)));
  }
  
  pthread_exit(NULL);
}

int main (int argc, char *argv[]) {
  int fd, ret;
  pthread_t t0, t1;
  
  fd = open(DEV_FILENAME, O_RDWR);
  if (fd == -1) {
    printf("Error opening file %s\n", DEV_FILENAME);
    exit (-1);
  }
  printf ("%s opened\n", DEV_FILENAME);
  
  ret = pthread_create(&t0, NULL, do_thread_work_write, (void *)(&fd));
  if (ret == -1) {
    fprintf(stderr, "ERROR; pthread_create() returned %s\n", strerror(ret));
    exit (-1);
  }
  ret = pthread_create(&t1, NULL, do_thread_work_read, (void *)(&fd));
  if (ret == -1) {
    fprintf(stderr, "ERROR; pthread_create() returned %s\n", strerror(ret));
    exit (-1);
  }
  
	pthread_join(t0, NULL);
	pthread_join(t1, NULL);
  
  close (fd);
  
  return 0;
}
