#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>

#define CDRV_IOC_MAGIC 'Z'
#define E2_IOCMODE1 _IOWR(CDRV_IOC_MAGIC, 1, int)
#define E2_IOCMODE2 _IOWR(CDRV_IOC_MAGIC, 2, int)

#define DEV_FILENAME "/dev/a5"

void * change_to_mode2(void *arg) {
  int *tid = (int *)arg;
  int fd;
  
  printf ("tid%d: Trying to open %s in mode 1\n", *tid, DEV_FILENAME);
  fd = open(DEV_FILENAME, O_RDWR);
  if (fd == -1) {
    printf("Error opening file %s\n", DEV_FILENAME);
    pthread_exit((void *)EXIT_FAILURE);
  }
  printf ("tid%d: %s opened in mode 1\n", *tid, DEV_FILENAME);
  
  sleep (5);
  
  printf ("tid%d: Trying to change mode to 2\n", *tid);
  ioctl(fd, E2_IOCMODE2, 2);
  
  close (fd);
  pthread_exit(NULL);
}

int main (int argc, char *argv[]) {
  int ret;
  pthread_t t0, t1;
  int tid0=0, tid1=1;
    
  ret = pthread_create(&t0, NULL, change_to_mode2, (void *)(&tid0));
  if (ret == -1) {
    fprintf(stderr, "ERROR; pthread_create() returned %s\n", strerror(ret));
    exit (-1);
  }
  ret = pthread_create(&t1, NULL, change_to_mode2, (void *)(&tid1));
  if (ret == -1) {
    fprintf(stderr, "ERROR; pthread_create() returned %s\n", strerror(ret));
    exit (-1);
  }
  
	pthread_join(t0, NULL);
	pthread_join(t1, NULL);
  
  return 0;
}
