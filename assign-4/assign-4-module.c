#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/semaphore.h>
#include <linux/uaccess.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/device.h>

struct asp_mycdrv { 
  struct list_head  list; 
  struct cdev       dev;
  char              *ramdisk;
  struct semaphore  sem; 
  int               devNo;
  int               direction;
  int               users;
};

#define DRIVER_NAME "assign-4-driver"

#define DBG_ALERT(fmt, args...) printk( KERN_ALERT "assign-4-driver: " fmt, ## args)
#define DBG_INFO(fmt, args...) printk( KERN_INFO "assign-4-driver: " fmt, ## args)

#define REGULAR 0
#define REVERSE 1

#define CDRV_IOC_MAGIC 'Z'
#define ASP_CHGACCDIR _IOWR(CDRV_IOC_MAGIC, 1, int)

#ifdef UNUSED
#define RAMDISK_SIZE    40
#else
#define RAMDISK_SIZE    (16 * PAGE_SIZE)
#endif

#define VALID_LO_OFFSET 0
#define VALID_HI_OFFSET (RAMDISK_SIZE - 1)

static int NUM_DEVICES = 3;
int i;
int ret;
dev_t dev_num;
struct asp_mycdrv *asp_mycdrvp = NULL;
static struct class *classp;
char temp;

static int device_open (struct inode *inodep, struct file *filep) {
	struct asp_mycdrv *device; 

	device = container_of(inodep->i_cdev, struct asp_mycdrv, dev);
	filep->private_data = device;
  
  // to protect device->users, direction
  if (down_interruptible(&device->sem))
    return -ERESTARTSYS;
  
  if (device->users) {
    
  } else {
    device->direction = REGULAR;
  }
  
  device->users++;
  
  DBG_INFO("device number %d opened, active users %d", device->devNo, device->users);
  
  up(&device->sem);
  return 0;
}

static void reverse (char *start, char *end) {
  while (start < end) {
    temp = *start;
    *start = *end;
    *end = temp;
    start++;
    end--;
  }
}

static ssize_t device_read (struct file *filep, char __user * buf, size_t lbuf, loff_t * ppos) {
  int nbytes, maxbytes, bytes_to_do;
  struct asp_mycdrv *device = filep->private_data;
  
  DBG_INFO("trying to read %d bytes from offset %ld from device %d in %d direction", lbuf, (long)(*ppos), device->devNo, device->direction);
  
  // to guard against using device->ramdisk when its being written to by someone else
  if (down_interruptible(&device->sem))
    return -ERESTARTSYS;
  
  if (REVERSE == device->direction) {
    maxbytes = *ppos + 1;
    maxbytes = maxbytes >= 0 && maxbytes <= RAMDISK_SIZE ? maxbytes : 0;
    bytes_to_do = maxbytes > lbuf ? lbuf : maxbytes;
    nbytes = bytes_to_do - copy_to_user(buf, (device->ramdisk) + *ppos - bytes_to_do + 1, bytes_to_do);
    reverse (buf, buf+nbytes-1);
    *ppos -= nbytes;
  } else {
    maxbytes = RAMDISK_SIZE - *ppos;
    maxbytes = maxbytes >= 0 && maxbytes <= RAMDISK_SIZE ? maxbytes : 0;
    bytes_to_do = maxbytes > lbuf ? lbuf : maxbytes;
    nbytes = bytes_to_do - copy_to_user(buf, (device->ramdisk) + *ppos, bytes_to_do);
  	*ppos += nbytes;
  }  
      
  DBG_INFO("read %d bytes", nbytes);
  
  up(&device->sem);
	return nbytes;
}

static ssize_t device_write (struct file *filep, const char __user * buf, size_t lbuf, loff_t * ppos) {
  int nbytes, maxbytes, bytes_to_do;
  struct asp_mycdrv *device = filep->private_data;
  
  DBG_INFO("trying to write %d bytes at offset %ld to device %d in direction %d", lbuf, (long)(*ppos), device->devNo, device->direction);

  // to protect device->ramdisk
	if (down_interruptible(&device->sem))
		return -ERESTARTSYS;
  
    if (REVERSE == device->direction) {
  	  maxbytes = *ppos + 1;
      maxbytes = maxbytes >= 0 && maxbytes <= RAMDISK_SIZE ? maxbytes : 0;
      bytes_to_do = maxbytes > lbuf ? lbuf : maxbytes;
      nbytes = bytes_to_do - copy_from_user((device->ramdisk) + *ppos - bytes_to_do + 1, buf, bytes_to_do);
      reverse ((device->ramdisk) + *ppos - bytes_to_do + 1, (device->ramdisk) + *ppos - bytes_to_do + 1 + nbytes-1);
      *ppos -= nbytes;
    } else {
    	maxbytes = RAMDISK_SIZE - *ppos;
      maxbytes = maxbytes >= 0 && maxbytes <= RAMDISK_SIZE ? maxbytes : 0;
    	bytes_to_do = maxbytes > lbuf ? lbuf : maxbytes;
    	nbytes = bytes_to_do - copy_from_user((device->ramdisk) + *ppos, buf, bytes_to_do);
    	*ppos += nbytes;
    }
  
  DBG_INFO("wrote %d bytes", nbytes);
  
  up(&device->sem);
  return nbytes;
}

static int device_close (struct inode *inodep, struct file *filep) {
  struct asp_mycdrv *device = filep->private_data;
  
  // to protect device->users
	if (down_interruptible(&device->sem))
		return -ERESTARTSYS;
  
  device->users--;
  DBG_INFO("device number %d closed, active users %d", device->devNo, device->users);
  
  up(&device->sem);
  return 0;
}

static loff_t device_lseek(struct file *filep, loff_t offset, int orig) {
	loff_t testpos;
  struct asp_mycdrv *device = filep->private_data;
      
	switch (orig) {
	  case SEEK_SET:
		  testpos = offset;
		  break;
      
	  case SEEK_CUR:
      testpos = filep->f_pos + offset; 
		  break;
      
	  case SEEK_END:
      testpos = RAMDISK_SIZE + offset; 
		  break;
      
	  default:
		  return -EINVAL;
	}
  
	testpos = testpos < RAMDISK_SIZE ? testpos : RAMDISK_SIZE;
	testpos = testpos >= 0 ? testpos : 0;
	filep->f_pos = testpos;
	DBG_INFO("lseek in device %d to pos %ld", device->devNo, (long)testpos);
  
  return testpos;
}

static long device_ioctl(struct file *filep, unsigned int cmd, unsigned long arg) {
  int old_direction, new_direction;
  struct asp_mycdrv *device = filep->private_data;
  
  // to protect device->direction
	if (down_interruptible(&device->sem))
		return -ERESTARTSYS;
  
  new_direction = *((int *)arg);
  old_direction = device->direction;
  
  DBG_INFO("ioctl for device %d, old direction %d, new direction %d", device->devNo, old_direction, new_direction);
  
  *((int *)arg) = old_direction;
  device->direction = new_direction;
  
  up(&device->sem);
  return 0;
}

struct file_operations fops = {
	.owner          = THIS_MODULE,
	.open           = device_open,
  .read           = device_read,
  .write          = device_write,
	.release        = device_close,
  .llseek         = device_lseek,
  .unlocked_ioctl = device_ioctl
};

static int driver_entry (void) {
  
  ret = alloc_chrdev_region(&dev_num, 0, NUM_DEVICES, DRIVER_NAME);
	if (ret < 0) {
    DBG_ALERT("failed to allocate major number");
    return ret;
  }
  
  // save major, minor numbers
  
  DBG_INFO("major number: %d", MAJOR(dev_num));
  
  if ( !(asp_mycdrvp = kmalloc(sizeof (struct asp_mycdrv) * NUM_DEVICES, GFP_KERNEL)) ) {
    DBG_ALERT("failed to allocate memory for asp_mycdrvp");
    return -ENOMEM;
  }
  
  classp = class_create (THIS_MODULE, "my_class00");
  
  for (i=0; i<NUM_DEVICES; i++) {
    
    cdev_init (&(asp_mycdrvp[i].dev), &fops);
    asp_mycdrvp[i].dev.ops = &fops;
    asp_mycdrvp[i].dev.owner = THIS_MODULE;
    asp_mycdrvp[i].devNo = i;
    asp_mycdrvp[i].direction = REGULAR;
    asp_mycdrvp[i].users = 0;
    
    ret = cdev_add(&(asp_mycdrvp[i].dev), MKDEV(MAJOR(dev_num), MINOR(dev_num) + i), 1);
    if (ret < 0) {
      DBG_ALERT("failed to add cdev to kernel");
      class_destroy(classp);
      kfree (asp_mycdrvp);
      unregister_chrdev_region(dev_num, NUM_DEVICES);
      return ret;
    }
    
    if ( !(asp_mycdrvp[i].ramdisk = kmalloc(RAMDISK_SIZE, GFP_KERNEL)) ) {
      DBG_ALERT("failed to allocate memory for ramdisk");
      class_destroy(classp);
      kfree (asp_mycdrvp);
      unregister_chrdev_region(dev_num, NUM_DEVICES);
      return -ENOMEM;
    }

    if ( IS_ERR(device_create (classp, NULL, MKDEV(MAJOR(dev_num), MINOR(dev_num) + i), NULL, "mycdrv%d", i)) ) {
      DBG_ALERT("failed to create device");
      // clean-up not correct!!
      // ramdisk should also be freed!!
      // cdev_del should be done??
      class_destroy (classp);
      kfree (asp_mycdrvp);
      unregister_chrdev_region (dev_num, NUM_DEVICES);
      return -ENXIO;
    }
    
    sema_init (&(asp_mycdrvp[i].sem), 1);
  }
  
  return 0;
}

static void driver_exit (void) {
  for (i=0; i<NUM_DEVICES; i++) {
    device_destroy (classp, MKDEV(MAJOR(dev_num), MINOR(dev_num) + i));
    kfree (asp_mycdrvp[i].ramdisk);
    // cdev_del (asp_mycdrvp[i].dev);
  }
  
  class_destroy (classp);  
  kfree (asp_mycdrvp);
  unregister_chrdev_region(dev_num, NUM_DEVICES);
  DBG_INFO ("unloaded module");
}

module_init (driver_entry);
module_exit (driver_exit);

module_param (NUM_DEVICES, int, S_IRUSR | S_IWUSR);

MODULE_AUTHOR("Joji Jacob");
MODULE_LICENSE ("GPL v2");