/*
 *  Copyright (c) 1999-2001 Vojtech Pavlik
 *
 *  USB HIDBP Keyboard support
 */

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Should you need to contact me, the author, you can do so either by
 * e-mail - mail your message to <vojtech@ucw.cz>, or by paper mail:
 * Vojtech Pavlik, Simunkova 1594, Prague 8, 182 00 Czech Republic
 */

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/usb/input.h>
#include <linux/hid.h>

/*
 * Version Information
 */
#define DRIVER_VERSION ""
#define DRIVER_AUTHOR "Vojtech Pavlik <vojtech@ucw.cz>"
#define DRIVER_DESC "USB HID Boot Protocol keyboard driver"
#define DRIVER_LICENSE "GPL"

MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
MODULE_LICENSE(DRIVER_LICENSE);

#define DBG_ALERT(fmt, args...) printk( KERN_ALERT "assign-6-driver: " fmt, ## args)
#define DBG_INFO(fmt, args...) printk( KERN_INFO "assign-6-driver: " fmt, ## args)

static const unsigned char usb_kbd_keycode[256] = {
	  0,  0,  0,  0, 30, 48, 46, 32, 18, 33, 34, 35, 23, 36, 37, 38,
	 50, 49, 24, 25, 16, 19, 31, 20, 22, 47, 17, 45, 21, 44,  2,  3,
	  4,  5,  6,  7,  8,  9, 10, 11, 28,  1, 14, 15, 57, 12, 13, 26,
	 27, 43, 43, 39, 40, 41, 51, 52, 53, 58, 59, 60, 61, 62, 63, 64,
	 65, 66, 67, 68, 87, 88, 99, 70,119,110,102,104,111,107,109,106,
	105,108,103, 69, 98, 55, 74, 78, 96, 79, 80, 81, 75, 76, 77, 71,
	 72, 73, 82, 83, 86,127,116,117,183,184,185,186,187,188,189,190,
	191,192,193,194,134,138,130,132,128,129,131,137,133,135,136,113,
	115,114,  0,  0,  0,121,  0, 89, 93,124, 92, 94, 95,  0,  0,  0,
	122,123, 90, 91, 85,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	 29, 42, 56,125, 97, 54,100,126,164,166,165,163,161,115,114,113,
	150,158,159,128,136,177,178,176,142,152,173,140
};

#define MODE1		1
#define MODE2		2

/**
 * struct usb_kbd - state of each attached keyboard
 * @dev:	input device associated with this keyboard
 * @usbdev:	usb device associated with this keyboard
 * @old:	data received in the past from the @irq URB representing which
 *		keys were pressed. By comparing with the current list of keys
 *		that are pressed, we are able to see key releases.
 * @irq:	URB for receiving a list of keys that are pressed when a
 *		new key is pressed or a key that was pressed is released.
 * @led:	URB for sending LEDs (e.g. numlock, ...)
 * @newleds:	data that will be sent with the @led URB representing which LEDs
 		should be on
 * @name:	Name of the keyboard. @dev's name field points to this buffer
 * @phys:	Physical path of the keyboard. @dev's phys field points to this
 *		buffer
 * @new:	Buffer for the @irq URB
 * @cr:		Control request for @led URB
 * @leds:	Buffer for the @led URB
 * @new_dma:	DMA address for @irq URB
 * @leds_dma:	DMA address for @led URB
 * @leds_lock:	spinlock that protects @leds, @newleds, and @led_urb_submitted
 * @led_urb_submitted: indicates whether @led is in progress, i.e. it has been
 *		submitted and its completion handler has not returned yet
 *		without	resubmitting @led
 * @mode:	Keyboard operating mode for assignment 6
 */
struct usb_kbd {
	struct input_dev *dev;
	struct usb_device *usbdev;
	unsigned char old[8];
	struct urb *irq, *led;
	unsigned char newleds;
	char name[128];
	char phys[64];

	unsigned char *new;
	struct usb_ctrlrequest *cr;
	unsigned char *leds;
	dma_addr_t new_dma;
	dma_addr_t leds_dma;
	
	spinlock_t leds_lock;
	bool led_urb_submitted;
	
	int mode;
	bool capslock_pressed;
	bool caps_locked;
};

static void usb_kbd_irq(struct urb *urb)
{
	unsigned long flags;
	struct usb_kbd *kbd = urb->context;
	int i;
	bool numlock_pressed = false;
	
	// DBG_INFO("usb_kbd_irq");

	switch (urb->status) {
	case 0:			/* success */
		break;
	case -ECONNRESET:	/* unlink */
	case -ENOENT:
	case -ESHUTDOWN:
		return;
	/* -EPIPE:  should clear the halt */
	default:		/* error */
		goto resubmit;
	}
	
	for (i = 0; i < 8; i++) {
		// 
		input_report_key(kbd->dev, usb_kbd_keycode[i + 224], (kbd->new[0] >> i) & 1);
	}

	for (i = 2; i < 8; i++) {
		// returns the address of the first occurrence of c, or 1 byte past the area if c is not found
		if (kbd->old[i] > 3 && memscan(kbd->new + 2, kbd->old[i], 6) == kbd->new + 8) {
			if (usb_kbd_keycode[kbd->old[i]]) {
				if (KEY_CAPSLOCK == usb_kbd_keycode[kbd->old[i]]) {
					DBG_INFO("Capslock has been released");
					kbd->capslock_pressed = false;
				}
				if ((KEY_NUMLOCK == usb_kbd_keycode[kbd->old[i]])) {
					DBG_INFO("Numlock has been released");
				}
				
				input_report_key(kbd->dev, usb_kbd_keycode[kbd->old[i]], 0);
			}
			else
				hid_info(urb->dev, "Unknown key (scancode %#x) released.\n", kbd->old[i]);
		}

		if (kbd->new[i] > 3 && memscan(kbd->old + 2, kbd->new[i], 6) == kbd->old + 8) {
			if (usb_kbd_keycode[kbd->new[i]]) {
				if (KEY_CAPSLOCK == usb_kbd_keycode[kbd->new[i]]) {
					DBG_INFO("Capslock has been pressed");
					kbd->capslock_pressed = true;
					kbd->caps_locked = !kbd->caps_locked;
					if (kbd->caps_locked)
						DBG_INFO("Caps is locked");
					else
						DBG_INFO("Caps is not locked");
				}
				if ((KEY_NUMLOCK == usb_kbd_keycode[kbd->new[i]])) {
					DBG_INFO("Numlock has been pressed");
					numlock_pressed = true;
				}
				
				input_report_key(kbd->dev, usb_kbd_keycode[kbd->new[i]], 1);
			}
			else
				hid_info(urb->dev, "Unknown key (scancode %#x) pressed.\n", kbd->new[i]);
		}
	}
	
	if (numlock_pressed) {
		if (MODE1 == kbd->mode && !kbd->caps_locked && !kbd->capslock_pressed) {
			DBG_INFO("Mode change - MODE1 to MODE2");
			kbd->mode = MODE2;
			
			// turn on capslock led when we move to mode 2
			spin_lock_irqsave(&kbd->leds_lock, flags);
			
			kbd->newleds = 	(!!test_bit(LED_KANA, kbd->dev->led) << 3) | 
							(!!test_bit(LED_COMPOSE, kbd->dev->led) << 3) | 
							(!!test_bit(LED_SCROLLL, kbd->dev->led) << 2) | 
							(true << 1) | 
							(!!test_bit(LED_NUML, kbd->dev->led));

			if (kbd->led_urb_submitted) {
				spin_unlock_irqrestore(&kbd->leds_lock, flags);
				goto resubmit;
			}

			if (*(kbd->leds) == kbd->newleds) {
				spin_unlock_irqrestore(&kbd->leds_lock, flags);
				goto resubmit;
			}
			
			*(kbd->leds) = kbd->newleds;
			
			kbd->led->dev = kbd->usbdev;
			if (usb_submit_urb(kbd->led, GFP_ATOMIC))
				pr_err("usb_submit_urb(leds) failed\n");
			else
				kbd->led_urb_submitted = true;
	
			spin_unlock_irqrestore(&kbd->leds_lock, flags);
		}
		else if (MODE2 == kbd->mode) {
			DBG_INFO("Mode change - MODE2 to MODE1");
			kbd->mode = MODE1;

			// change capslock led to caps status in input layer status
			spin_lock_irqsave(&kbd->leds_lock, flags);
			
			kbd->newleds = 	(!!test_bit(LED_KANA, kbd->dev->led) << 3) | 
							(!!test_bit(LED_COMPOSE, kbd->dev->led) << 3) | 
							(!!test_bit(LED_SCROLLL, kbd->dev->led) << 2) | 
							(!!test_bit(LED_CAPSL, kbd->dev->led) << 1) | 
							(!!test_bit(LED_NUML, kbd->dev->led));

			if (kbd->led_urb_submitted) {
				spin_unlock_irqrestore(&kbd->leds_lock, flags);
				goto resubmit;
			}

			if (*(kbd->leds) == kbd->newleds) {
				spin_unlock_irqrestore(&kbd->leds_lock, flags);
				goto resubmit;
			}
			
			*(kbd->leds) = kbd->newleds;
			
			kbd->led->dev = kbd->usbdev;
			if (usb_submit_urb(kbd->led, GFP_ATOMIC))
				pr_err("usb_submit_urb(leds) failed\n");
			else
				kbd->led_urb_submitted = true;
	
			spin_unlock_irqrestore(&kbd->leds_lock, flags);
		}
	}

	// call to tell those who receive the events that we've sent a complete report
	input_sync(kbd->dev);

	memcpy(kbd->old, kbd->new, 8);

resubmit:
	i = usb_submit_urb (urb, GFP_ATOMIC);
	if (i)
		hid_err(urb->dev, "can't resubmit intr, %s-%s/input0, status %d", kbd->usbdev->bus->bus_name, kbd->usbdev->devpath, i);

}

static int usb_kbd_event(struct input_dev *dev, unsigned int type,
			 unsigned int code, int value)
{
	unsigned long flags;
	struct usb_kbd *kbd = input_get_drvdata(dev);
	
	// DBG_INFO("usb_kbd_event");

	if (type != EV_LED)
		return -1;

	spin_lock_irqsave(&kbd->leds_lock, flags);
	if (MODE1 == kbd->mode)
		kbd->newleds = 	(!!test_bit(LED_KANA, dev->led) << 3) | 
						(!!test_bit(LED_COMPOSE, dev->led) << 3) | 
						(!!test_bit(LED_SCROLLL, dev->led) << 2) | 
						(!!test_bit(LED_CAPSL, dev->led) << 1) | 
						(!!test_bit(LED_NUML, dev->led));
	else
		kbd->newleds = 	(!!test_bit(LED_KANA, dev->led) << 3) | 
						(!!test_bit(LED_COMPOSE, dev->led) << 3) | 
						(!!test_bit(LED_SCROLLL, dev->led) << 2) | 
						(!test_bit(LED_CAPSL, dev->led) << 1) | 
						(!!test_bit(LED_NUML, dev->led));

	if (kbd->led_urb_submitted){
		spin_unlock_irqrestore(&kbd->leds_lock, flags);
		return 0;
	}

	if (*(kbd->leds) == kbd->newleds){
		spin_unlock_irqrestore(&kbd->leds_lock, flags);
		return 0;
	}

	*(kbd->leds) = kbd->newleds;
	
	kbd->led->dev = kbd->usbdev;
	if (usb_submit_urb(kbd->led, GFP_ATOMIC))
		pr_err("usb_submit_urb(leds) failed\n");
	else
		kbd->led_urb_submitted = true;
	
	spin_unlock_irqrestore(&kbd->leds_lock, flags);
	
	return 0;
}

static void usb_kbd_led(struct urb *urb)
{
	unsigned long flags;
	struct usb_kbd *kbd = urb->context;
	
	// DBG_INFO("usb_kbd_led");

	if (urb->status)
		hid_warn(urb->dev, "led urb status %d received\n", urb->status);

	spin_lock_irqsave(&kbd->leds_lock, flags);

	if (*(kbd->leds) == kbd->newleds){
		kbd->led_urb_submitted = false;
		spin_unlock_irqrestore(&kbd->leds_lock, flags);
		return;
	}

	*(kbd->leds) = kbd->newleds;
	
	kbd->led->dev = kbd->usbdev;
	if (usb_submit_urb(kbd->led, GFP_ATOMIC)){
		hid_err(urb->dev, "usb_submit_urb(leds) failed\n");
		kbd->led_urb_submitted = false;
	}
	spin_unlock_irqrestore(&kbd->leds_lock, flags);
	
}

static int usb_kbd_open(struct input_dev *dev)
{
	struct usb_kbd *kbd = input_get_drvdata(dev);
	
	// DBG_INFO("usb_kbd_open");

	kbd->irq->dev = kbd->usbdev;
	if (usb_submit_urb(kbd->irq, GFP_KERNEL))
		return -EIO;
	
	// DBG_INFO("irq urb submitted from open");

	return 0;
}

static void usb_kbd_close(struct input_dev *dev)
{
	struct usb_kbd *kbd = input_get_drvdata(dev);
	
	DBG_INFO("usb_kbd_close");

	usb_kill_urb(kbd->irq);
}

static int usb_kbd_alloc_mem(struct usb_device *dev, struct usb_kbd *kbd)
{
	if (!(kbd->irq = usb_alloc_urb(0, GFP_KERNEL)))
		return -1;
	if (!(kbd->led = usb_alloc_urb(0, GFP_KERNEL)))
		return -1;
	
	// @dev = the usb device (that we got from paramter to probe)
	// @new_dma = the dma address of buffer
	// TODO - where/how is this used
	// @new = cpu-space pointer to a buffer that may be used to perform DMA to the specified device
	// TODO - where/how is this used
	if (!(kbd->new = usb_alloc_coherent(dev, 8, GFP_ATOMIC, &kbd->new_dma)))
		return -1;
	
	// @cr = will be used for sending control requests to usb device
	// TODO - where/how is this used
	// ?? = doesn't it need to bind some control port
	if (!(kbd->cr = kmalloc(sizeof(struct usb_ctrlrequest), GFP_KERNEL)))
		return -1;
	if (!(kbd->leds = usb_alloc_coherent(dev, 1, GFP_ATOMIC, &kbd->leds_dma)))
		return -1;

	return 0;
}

static void usb_kbd_free_mem(struct usb_device *dev, struct usb_kbd *kbd)
{
	usb_free_urb(kbd->irq);
	usb_free_urb(kbd->led);
	usb_free_coherent(dev, 8, kbd->new, kbd->new_dma);
	kfree(kbd->cr);
	usb_free_coherent(dev, 1, kbd->leds, kbd->leds_dma);
}

static int usb_kbd_probe(struct usb_interface *iface,
			 const struct usb_device_id *id)
{
	// get the usb device struct from iface
	struct usb_device *dev = interface_to_usbdev(iface);
	struct usb_host_interface *interface;
	struct usb_endpoint_descriptor *endpoint;
	struct usb_kbd *kbd;
	// this input device struct will be allocated with input_allocate_device
	struct input_dev *input_dev;
	int i, pipe, maxp;
	int error = -ENOMEM;
	
	DBG_INFO("usb_kbd_probe");
	
	// interface to usb host (from iface)
	interface = iface->cur_altsetting;

	if (interface->desc.bNumEndpoints != 1)
		return -ENODEV;
	
	// get the endpoint desc from usb host interface
	endpoint = &interface->endpoint[0].desc;
	// check if the endpoint is interrupt IN
	if (!usb_endpoint_is_int_in(endpoint))
		return -ENODEV;
	
	// create a pipe of type receive interrupt; other types are send interrupt, receive/send bulk, isochronous
	// like an fd
	pipe = usb_rcvintpipe(dev, endpoint->bEndpointAddress);
	// max packet size of this endpoint
	// this is just endian conversion; max size is present in usb_endpoint_descriptor->wMaxPacketSize
	maxp = usb_maxpacket(dev, pipe, usb_pipeout(pipe));

	kbd = kzalloc(sizeof(struct usb_kbd), GFP_KERNEL);
	input_dev = input_allocate_device();
	if (!kbd || !input_dev)
		goto fail1;
	
	// alloc urb, coherent dma buffer, control request struct
	if (usb_kbd_alloc_mem(dev, kbd))
		goto fail2;

	// init'ing kbd parameters
	kbd->usbdev = dev;
	kbd->dev = input_dev;
	spin_lock_init(&kbd->leds_lock);

	// ++being very careful in setting the name!!!
	if (dev->manufacturer)
		strlcpy(kbd->name, dev->manufacturer, sizeof(kbd->name));

	if (dev->product) {
		if (dev->manufacturer)
			strlcat(kbd->name, " ", sizeof(kbd->name));
		strlcat(kbd->name, dev->product, sizeof(kbd->name));
	}

	if (!strlen(kbd->name))
		snprintf(kbd->name, sizeof(kbd->name), "USB HIDBP Keyboard %04x:%04x", le16_to_cpu(dev->descriptor.idVendor), le16_to_cpu(dev->descriptor.idProduct));
	// --being very careful in setting the name!!!
			 
	// returns stable device path in the usb tree
	// reflects physical paths in hardware such as physical bus addresses for host controllers or ports on USB hubs
	// copies some string into phys
	usb_make_path(dev, kbd->phys, sizeof(kbd->phys));
	strlcat(kbd->phys, "/input0", sizeof(kbd->phys));

	// init'ing input device parameters
	input_dev->name = kbd->name;
	input_dev->phys = kbd->phys;
	usb_to_input_id(dev, &input_dev->id);
	input_dev->dev.parent = &iface->dev;

	// embedding our device structure in input device
	input_set_drvdata(input_dev, kbd);

	// @evbit = bitmap of events supported by the device
	input_dev->evbit[0] = BIT_MASK(EV_KEY) | BIT_MASK(EV_LED) | BIT_MASK(EV_REP);
	// @ledbit = bitmap of leds present on the device
	input_dev->ledbit[0] = BIT_MASK(LED_NUML) | BIT_MASK(LED_CAPSL) | BIT_MASK(LED_SCROLLL) | BIT_MASK(LED_COMPOSE) | BIT_MASK(LED_KANA);

	// @keybit = bitmap of keys/buttons this device has
	for (i = 0; i < 255; i++)
		set_bit(usb_kbd_keycode[i], input_dev->keybit);
	clear_bit(0, input_dev->keybit);

	// event handler for events sent _to_ the device, like EV_LED or EV_SND. 
	// The device is expected to carry out the requested action (turn on a LED, play sound, etc.)
	input_dev->event = usb_kbd_event;
	// this method will be called when the very first user calls input_open_device. 
	// The driver must prepare the device to start generating events (start polling thread, "request an IRQ", "submit URB", etc.)
	input_dev->open = usb_kbd_open;
	// this method will be called when the very last user calls input_close_device
	input_dev->close = usb_kbd_close;

	// initialize an interrupt urb
	usb_fill_int_urb(kbd->irq, dev, pipe, kbd->new, (maxp > 8 ? 8 : maxp), usb_kbd_irq, kbd, endpoint->bInterval);
	kbd->irq->transfer_dma = kbd->new_dma;
	kbd->irq->transfer_flags |= URB_NO_TRANSFER_DMA_MAP;

	// fill out the control request
	kbd->cr->bRequestType = USB_TYPE_CLASS | USB_RECIP_INTERFACE;
	kbd->cr->bRequest = 0x09;
	kbd->cr->wValue = cpu_to_le16(0x200);
	kbd->cr->wIndex = cpu_to_le16(interface->desc.bInterfaceNumber);
	kbd->cr->wLength = cpu_to_le16(1);

	// initialize a control urb
	usb_fill_control_urb(kbd->led, dev, usb_sndctrlpipe(dev, 0), (void *) kbd->cr, kbd->leds, 1, usb_kbd_led, kbd);
	kbd->led->transfer_dma = kbd->leds_dma;
	kbd->led->transfer_flags |= URB_NO_TRANSFER_DMA_MAP;
	
	// init mode and other assignment stuff
	kbd->mode = MODE1;
	kbd->capslock_pressed = false;
	kbd->caps_locked = false;

	// register input device with input core
	error = input_register_device(kbd->dev);
	if (error)
		goto fail2;

	// embedding our device structure in iface
	usb_set_intfdata(iface, kbd);
	// enable device to wake up the system
	device_set_wakeup_enable(&dev->dev, 1);
	return 0;

fail2:	
	usb_kbd_free_mem(dev, kbd);
fail1:	
	input_free_device(input_dev);
	kfree(kbd);
	return error;
}

static void usb_kbd_disconnect(struct usb_interface *intf)
{
	struct usb_kbd *kbd = usb_get_intfdata (intf);
	
	DBG_INFO("usb_kbd_disconnect");

	usb_set_intfdata(intf, NULL);
	if (kbd) {
		usb_kill_urb(kbd->irq);
		input_unregister_device(kbd->dev);
		usb_kill_urb(kbd->led);
		usb_kbd_free_mem(interface_to_usbdev(intf), kbd);
		kfree(kbd);
	}
}

static struct usb_device_id usb_kbd_id_table [] = {
	{ USB_INTERFACE_INFO(USB_INTERFACE_CLASS_HID, USB_INTERFACE_SUBCLASS_BOOT,
		USB_INTERFACE_PROTOCOL_KEYBOARD) },
	{ }						/* Terminating entry */
};

MODULE_DEVICE_TABLE (usb, usb_kbd_id_table);

static struct usb_driver usb_kbd_driver = {
	.name =		"usbkbd",
	.probe =	usb_kbd_probe,
	.disconnect =	usb_kbd_disconnect,
	.id_table =	usb_kbd_id_table,
};

module_usb_driver(usb_kbd_driver);
