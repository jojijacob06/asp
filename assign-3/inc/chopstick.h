#ifndef CHOPSTICK_H
#define CHOPSTICK_H

#include <pthread.h>

typedef struct chopstick {
  pthread_mutex_t lock;
  pthread_cond_t nowAvailable;
  int available;
} chopstick_t;

chopstick_t *chopstick_create(char *chopstick_name, int maxProcesses);
chopstick_t *chopstick_open(char *chopstick_name);
void chopstick_lock(chopstick_t *c);
void chopstick_unlock(chopstick_t *c);
int chopstick_is_available(chopstick_t *c);
void chopstick_set_available(chopstick_t *c);
void chopstick_set_unavailable(chopstick_t *c);
void chopstick_wait_for_left_one(chopstick_t *c);

#endif