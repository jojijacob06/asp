#ifndef PROCESS_ID_H
#define PROCESS_ID_H

#include <pthread.h>

typedef struct process_id {
  pthread_mutex_t lock;
  int count;
  int maxProcesses;
} process_id_t;

process_id_t *process_id_create(char *process_id_name, int maxProcesses);
process_id_t *process_id_open(char *process_id_name);
int process_id_get_new_id(process_id_t *p);
int process_id_get_max_processes(process_id_t *p);
void process_id_close(process_id_t *p);

#endif