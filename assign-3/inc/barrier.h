#ifndef BARRIER_H
#define BARRIER_H

#include <pthread.h>

typedef struct barrier {
  pthread_mutex_t lock;
  pthread_cond_t zero;
  int count;
  int maxProcesses;
} barrier_t;

barrier_t *barrier_create(char *barrier_name, int maxProcesses);
barrier_t *barrier_open(char *barrier_name);
void barrier_wait(barrier_t *b);
void barrier_close(barrier_t *b);

#endif