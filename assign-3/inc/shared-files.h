#ifndef SHARED_FILES_H
#define SHARED_FILES_H

#define PROCESS_ID_SHARED_FILE  "../res/process_id"
#define BARRIER_SHARED_FILE     "../res/barrier"
#define CHOPSTICKS_SHARED_FILE  "../res/chopsticks"

#endif