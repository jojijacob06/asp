#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <pthread.h>
#include "chopstick.h"

chopstick_t *chopstick_create(char *chopstick_name, int maxProcesses) {
  int fd;
  chopstick_t *c;
  pthread_mutexattr_t psharedm;
  pthread_condattr_t psharedc;

  fd = open(chopstick_name, O_RDWR | O_CREAT | O_TRUNC, 0666);
  if (fd < 0)
      return (NULL);
  (void) ftruncate(fd, sizeof(chopstick_t) * maxProcesses);
  
  (void) pthread_mutexattr_init(&psharedm);
  (void) pthread_mutexattr_setpshared(&psharedm, PTHREAD_PROCESS_SHARED);
  (void) pthread_condattr_init(&psharedc);
  (void) pthread_condattr_setpshared(&psharedc, PTHREAD_PROCESS_SHARED);
      
  c = (chopstick_t *) mmap(NULL, sizeof(chopstick_t) * maxProcesses,
          PROT_READ | PROT_WRITE, MAP_SHARED,
          fd, 0);
  close (fd);
  
  for (int i=0; i<maxProcesses; i++) {
    (void) pthread_mutex_init(&((c+i)->lock), &psharedm);
    (void) pthread_cond_init(&((c+i)->nowAvailable), &psharedc);
    (c+i)->available = 1;
  }
  return (c);  
}

chopstick_t *chopstick_open(char *chopstick_name) {
  int fd;
  chopstick_t *c;

  fd = open(chopstick_name, O_RDWR, 0666);
  if (fd < 0)
      return (NULL);
  off_t fsize = lseek(fd, 0, SEEK_END);
  lseek(fd, 0, SEEK_SET);
  c = (chopstick_t *) mmap(NULL, (size_t)fsize,
          PROT_READ | PROT_WRITE, MAP_SHARED,
          fd, 0);
  close (fd);
  return (c);
}

void chopstick_lock(chopstick_t *c) {
  pthread_mutex_lock (&c->lock);
}

void chopstick_unlock(chopstick_t *c) {
  pthread_mutex_unlock (&c->lock);
}

int chopstick_is_available(chopstick_t *c) {
  return c->available;
}

void chopstick_set_available(chopstick_t *c) {
  c->available = 1;
  pthread_cond_broadcast(&c->nowAvailable);
}

void chopstick_set_unavailable(chopstick_t *c) {
  c->available = 0;
}

void chopstick_wait_for_left_one(chopstick_t *c) {
  pthread_mutex_lock (&c->lock);
  while (0 == c->available)
    pthread_cond_wait(&c->nowAvailable, &c->lock);
}