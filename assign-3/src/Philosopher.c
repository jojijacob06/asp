#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <inttypes.h>

#include "process-id.h"
#include "barrier.h"
#include "chopstick.h"

#include "shared-files.h"

int main (int argc, char *argv[]) {
  int numberOfMeals = strtoimax(argv[1], NULL, 10);
  if (errno) {
    fprintf (stderr, "Bad arguments(s) - %s\n", strerror(errno));
    exit (EXIT_FAILURE);
  }
  
  process_id_t *p = process_id_open (PROCESS_ID_SHARED_FILE);
  if (NULL == p)
    exit (1);
  
  barrier_t *b = barrier_open (BARRIER_SHARED_FILE);
  if (NULL == b) 
    exit (1);
  
  chopstick_t *c = chopstick_open (CHOPSTICKS_SHARED_FILE);
  if (NULL == c) 
    exit (1);
  
  int id = process_id_get_new_id(p);
  int maxPhils = process_id_get_max_processes(p);
  process_id_close(p);
    
  printf ("Hi, I'm philosopher%d\n", id);
  
  barrier_wait (b);
  
  printf ("philosopher%d is THINKING\n", id);
  
  for (int i=0; i<numberOfMeals; i++) {
            
    // thinking  
    usleep (rand() % 300000);
    printf ("philosopher%d is HUNGRY\n", id);
    
    int gotBothChopsticks = 0;
    while (!gotBothChopsticks) {
      chopstick_wait_for_left_one (c+id);
      // we have the left chopstick now
      chopstick_set_unavailable (c+id);
      chopstick_unlock (c+id);
      
      // try to get the right chopstick
      chopstick_lock (c+ ((id+1)%maxPhils));
      
      if (chopstick_is_available (c+ ((id+1)%maxPhils))) {
        // we got the right one too
        chopstick_set_unavailable (c+ ((id+1)%maxPhils));
        chopstick_unlock (c+ ((id+1)%maxPhils));
          
        gotBothChopsticks = 1;
      }
      else {
        // we didn't get the right chopstick
        chopstick_unlock (c+ ((id+1)%maxPhils));
        
        chopstick_lock (c+id);
        chopstick_set_available (c+id);
        chopstick_unlock (c+id);
      }
    }
    
    printf ("philosopher%d is EATING\n", id);
    usleep(rand() % 300000);
    chopstick_lock (c+id);
    chopstick_lock (c+ ((id+1)%maxPhils));
    chopstick_set_available (c+id);
    chopstick_set_available (c+ ((id+1)%maxPhils));
    chopstick_unlock (c+id);
    chopstick_unlock (c+ ((id+1)%maxPhils));
    printf ("philosopher%d is THINKING\n", id);
    
  }
  
  barrier_wait (b);
  
  printf ("philosopher%d is done.\n", id);
  
  barrier_close (b);
  
  return 0;
}