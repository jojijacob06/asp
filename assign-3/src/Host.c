#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <inttypes.h>

#include "process-id.h"
#include "barrier.h"
#include "chopstick.h"

#include "shared-files.h"

char *philosopher_exec[] = {"./Philosopher", 0, 0};

int main (int argc, char *argv[]) {
  int status;
  
  if (3 != argc) {
    fprintf (stderr, "Wrong usage!\n%s <N=number of philosophers> <M=number of iterations>\n", argv[0]);
    exit (1);
  }
    
  int numberOfPhils = strtoimax(argv[1], NULL, 10);
  if (errno) {
    fprintf (stderr, "Bad arguments(s) - %s\n", strerror(errno));
    exit (EXIT_FAILURE);
  }
  
  philosopher_exec[1] = argv[2];
  
  process_id_t *p = process_id_create (PROCESS_ID_SHARED_FILE, numberOfPhils);
  if (NULL == p)
    exit (1);
  process_id_close (p);
  
  barrier_t *b = barrier_create (BARRIER_SHARED_FILE, numberOfPhils);
  if (NULL == b) 
    exit (1);
  barrier_close (b);
  
  chopstick_t *c = chopstick_create (CHOPSTICKS_SHARED_FILE, numberOfPhils);
  if (NULL == c) 
    exit (1);
  
  pid_t pid;
  
  for (int i=0; i<numberOfPhils; i++) {
    
    switch (pid = fork()) {
      case -1:
        fprintf (stderr, "Fork failed.\n");
        exit (EXIT_FAILURE);
        break;
        
      case 0:
        if (execvp (philosopher_exec[0], philosopher_exec)) {
          fprintf (stderr, "Execvp failed trying to execute Philosopher(%s).\n", strerror(errno));
          exit (EXIT_FAILURE);
        }
        exit (0);
        break;
    }
  }
  
  while ((pid = wait(&status)) != -1) {
/*
        if (WIFEXITED(status))
      fprintf(stderr, "%d exited with %d\n", pid, WEXITSTATUS(status));
    else if (WIFSIGNALED(status))
      fprintf(stderr, "%d was terminated by %s\n", pid, strsignal(WTERMSIG(status)));
*/
  }
  
  printf ("all philosophers finished. host finishing ...\n");
  
  return 0;
}