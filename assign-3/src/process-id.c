#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <pthread.h>
#include "process-id.h"

process_id_t *process_id_create(char *process_id_name, int maxProcesses) {
  int fd;
  process_id_t *p;
  pthread_mutexattr_t psharedm;

  fd = open(process_id_name, O_RDWR | O_CREAT | O_TRUNC, 0666);
  if (fd < 0)
      return (NULL);
  
  ftruncate(fd, sizeof(process_id_t));
  pthread_mutexattr_init(&psharedm);
  pthread_mutexattr_setpshared(&psharedm, PTHREAD_PROCESS_SHARED);
  
  p = (process_id_t *) mmap(NULL, sizeof(process_id_t),
          PROT_READ | PROT_WRITE, MAP_SHARED,
          fd, 0);
  close (fd);
  pthread_mutex_init(&p->lock, &psharedm);
  p->count = 0;
  p->maxProcesses = maxProcesses;
  return (p);
}

process_id_t *process_id_open(char *process_id_name) {
  int fd;
  process_id_t *p;

  fd = open(process_id_name, O_RDWR, 0666);
  if (fd < 0)
      return (NULL);
  p = (process_id_t *) mmap(NULL, sizeof(process_id_t),
          PROT_READ | PROT_WRITE, MAP_SHARED,
          fd, 0);
  close (fd);
  return (p);
}

int process_id_get_new_id(process_id_t *p) {
  int old_count;
  pthread_mutex_lock(&p->lock);
  old_count = p->count;
  p->count++;
  pthread_mutex_unlock(&p->lock);
  return old_count;
}

int process_id_get_max_processes(process_id_t *p) {
  return p->maxProcesses;
}

void process_id_close(process_id_t *p) {
  munmap((void *) p, sizeof(process_id_t));
}