#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <pthread.h>
#include "barrier.h"

barrier_t *barrier_create(char *barrier_name, int maxProcesses) {
  int fd;
  barrier_t *b;
  pthread_mutexattr_t psharedm;
  pthread_condattr_t psharedc;

  fd = open(barrier_name, O_RDWR | O_CREAT | O_TRUNC, 0666);
  if (fd < 0)
      return (NULL);
  (void) ftruncate(fd, sizeof(barrier_t));
  (void) pthread_mutexattr_init(&psharedm);
  (void) pthread_mutexattr_setpshared(&psharedm,
      PTHREAD_PROCESS_SHARED);
  (void) pthread_condattr_init(&psharedc);
  (void) pthread_condattr_setpshared(&psharedc,
      PTHREAD_PROCESS_SHARED);
  b = (barrier_t *) mmap(NULL, sizeof(barrier_t),
          PROT_READ | PROT_WRITE, MAP_SHARED,
          fd, 0);
  close (fd);
  (void) pthread_mutex_init(&b->lock, &psharedm);
  (void) pthread_cond_init(&b->zero, &psharedc);
  b->maxProcesses = maxProcesses;
  b->count = b->maxProcesses;
  return (b);  
}

barrier_t *barrier_open(char *barrier_name) {
  int fd;
  barrier_t *b;

  fd = open(barrier_name, O_RDWR, 0666);
  if (fd < 0)
      return (NULL);
  b = (barrier_t *) mmap(NULL, sizeof(barrier_t),
          PROT_READ | PROT_WRITE, MAP_SHARED,
          fd, 0);
  close (fd);
  return (b);
}

void barrier_wait(barrier_t *b) {
  pthread_mutex_lock(&b->lock);
  b->count--;
  
  if (b->count > 0) {
    while (pthread_cond_wait(&b->zero, &b->lock));
  }
  else {
    b->count = b->maxProcesses;
    pthread_cond_broadcast(&b->zero);
  }
  pthread_mutex_unlock(&b->lock);
}

void barrier_close(barrier_t *b) {
  munmap((void *) b, sizeof(barrier_t));
}