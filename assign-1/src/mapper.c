#include <stdio.h>
#include <errno.h>    // for errno
#include <stdlib.h>   // for exit
#include <string.h>   // for strerror

#include "KeyValuePair.h"

#define MAX_KEY_SIZE  100
#define DEFAULT_WORDS_FILE  "../res/words.txt"

// for use in scanf
#define XSTR(A) STR(A)
#define STR(A) #A

int main (int argc, char *argv[]) {
  char key[MAX_KEY_SIZE+1];
  FILE *fp;
  char *filename=NULL;
  
  if (2 == argc)
    filename = argv[1];
  else
    filename = DEFAULT_WORDS_FILE;
  
  if (!(fp = fopen(filename, "r"))) {
    fprintf(stderr, "Fopen failed to open %s(%s).\n", filename, strerror(errno));
    exit(EXIT_FAILURE);
  }

  while (EOF != fscanf(fp, "%"XSTR(MAX_KEY_SIZE)"s", key)) {
    KeyValuePair_t *pKeyValuePair = makeNewKeyValuePair(key, 1);
    prettyPrintKeyValuePair(pKeyValuePair);
    freeKeyValuePair(pKeyValuePair);
  }

  return 0;
}
