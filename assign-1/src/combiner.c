#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

char *mapper_exec[] = {"./mapper", 0, 0};
char *reducer_exec[] = {"./reducer", 0};

char *mapper_name = "mapper";
char *reducer_name = "reducer";
char *empty_name = "";

pid_t mapper_pid = -1;
pid_t reducer_pid = -1;

void runMapper(int fd_out[]) {
  pid_t pid;
  
  switch(pid = fork()) {
    case -1:
      fprintf(stderr, "Fork failed.\n");
      exit(EXIT_FAILURE);
      break;
    
    case 0:
      close(fd_out[0]);
      dup2(fd_out[1], 1);
      if (execvp(mapper_exec[0], mapper_exec)) {
        fprintf(stderr, "Execvp failed trying to execute mapper(%s).\n", strerror(errno));
        exit(EXIT_FAILURE);
      }
      exit(0);
      break;
    
    default:
      mapper_pid = pid;
      break;
  }
}

void runReducer(int fd_mapper_out[]) {
  pid_t pid;
  
  switch(pid = fork()) {
    case -1:
      fprintf(stderr, "Fork failed.\n");
      exit(EXIT_FAILURE);
      break;
      
    case 0:
      close(fd_mapper_out[1]);
      dup2(fd_mapper_out[0], 0);
      if (execvp(reducer_exec[0], reducer_exec)) {
        fprintf(stderr, "Execvp failed trying to execute reducer(%s).\n", strerror(errno));
        exit(EXIT_FAILURE);
      }        
      exit(0);
      break;
      
    default:
      reducer_pid = pid;
      close(fd_mapper_out[0]);
      close(fd_mapper_out[1]);
      break;
    
  }
}

int main(int argc, char *argv[]) {
  pid_t pid;
  int status;
  int fd_mapper_out[2];
  char *who=NULL;
    
  if (2==argc)
    mapper_exec[1] = argv[1];
    
  if(pipe(fd_mapper_out)) {
    fprintf(stderr, "Pipe failed.\n");
    return EXIT_FAILURE;
  }
  
  runMapper(fd_mapper_out);
  runReducer(fd_mapper_out);
  
  while ((pid = wait(&status)) != -1) {
    if (mapper_pid == pid)
      who = mapper_name;
    else if (reducer_pid == pid)
      who = reducer_name;
    else
      who = empty_name;
    if (WIFEXITED(status))
      fprintf(stderr, "%s exited with %d\n", who, WEXITSTATUS(status));
    else if (WIFSIGNALED(status))
      fprintf(stderr, "%s was terminated by %s\n", who, strsignal(WTERMSIG(status)));
  }
  
  return 1;
}