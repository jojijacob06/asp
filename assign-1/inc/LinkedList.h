#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include "KeyValuePair.h"

typedef struct LinkedList {
  KeyValuePair_t *pKeyValuePair;
  struct LinkedList *next;
}LinkedList_t;

void addToListAtHead(LinkedList_t **head, LinkedList_t **tail, KeyValuePair_t *pNewKeyValuePair);
void addToListAtTail(LinkedList_t **head, LinkedList_t **tail, KeyValuePair_t *pNewKeyValuePair);
void freeList(LinkedList_t **head, LinkedList_t **tail);
void printList(LinkedList_t *head);

#endif